from pygame.locals import *

import pygame
from numpy import random

random.seed(10000)

size = 32

world_width = 100 * size
world_height = 100 * size

brain_size = 64

DIRECTIONS = [
    (-1, -1),
    (0, -1),
    (1, -1),
    (1, 0),
    (1, 1),
    (0, 1),
    (-1, 1),
    (-1, 0),
]

ACID = 1
WALL = 2
BOT = 3
FOOD = 4
NOTHING = 5


# map = [1] * brain_size
class GameObject:
    type: int

    def __init__(self, type):
        self.type = type

    def decide_move(self):
        pass

    def __str__(self):
        return self.type


class Bot:
    def __init__(self, field: list, brain: list, x=None, y=None):
        self.field = field
        self.type = BOT
        self.brain = brain.copy()
        self.x = x or random.randint(0, world_width)
        self.y = y or random.randint(0, world_height)
        self.field[self.y][self.x] = self
        self.cur_pos = 0
        self.energy = 40
        self.direction = random.randint(0, 8)

    def decide_move(self):
        count = 10
        while count > 0:
            count -= 1
            action = self.brain[self.cur_pos]
            if action < 8:
                self.move(action)
                break
            elif action < 16:
                self.take(action - 8)
                break
            elif action < 24:
                self.look(action - 16)
            elif action < 32:
                self.direction += action - 24
                self.cur_pos += 1
            else:
                self.cur_pos += action + 1

            self.direction %= 8
            self.cur_pos %= brain_size

        self.energy -= 1

    def look(self, value):
        direction = DIRECTIONS[(self.direction + value) % 8]
        self.cur_pos += self.field[direction[1]][direction[0]].type

        self.cur_pos %= brain_size

    def take(self, value):
        direction = DIRECTIONS[(self.direction + value) % 8]
        type_ = self.field[direction[1]][direction[0]].type

        if type_ == ACID:
            self.field[direction[1]][direction[0]] = GameObject(FOOD)
        # if type_ == BOT:
        #     self.field[direction[1]][direction[0]] = GameObject(FOOD)
        elif type_ == FOOD:
            self.field[direction[1]][direction[0]] = GameObject(NOTHING)
            self.energy += 10

        self.cur_pos += type_

        self.cur_pos %= brain_size

    def move(self, value):
        direction = DIRECTIONS[(self.direction + value) % 8]
        type_ = self.field[direction[1]][direction[0]].type

        if type_ in (ACID, FOOD, NOTHING):
            if type_ == ACID:
                self.energy = 0

            elif type_ == FOOD:
                self.energy += 10

            self.x += direction[0]
            self.y += direction[1]
            self.field[self.y][self.x] = self

        self.cur_pos += type_

        self.cur_pos %= brain_size


FIELD = [
    [GameObject(WALL)] * int(world_width/size),
    *[[GameObject(WALL)] + [GameObject(NOTHING)]*int(world_width/size-2) + [GameObject(WALL)] for x in range(int(world_height/size-2))],
    [GameObject(WALL)] * int(world_width/size),
]

print(FIELD)

class App:
    windowWidth = 800
    windowHeight = 600
    player = 0

    def __init__(self):
        self.field = FIELD
        self._running = True
        self._display_surf = None
        self._image_surf = None
        self.bots = [
            Bot(self.field, [17, 9, 1, 61, 1, 1, 26, 57]),
            Bot(self.field, [17, 9, 1, 61, 1, 1, 26, 57]),
            Bot(self.field, [17, 9, 1, 61, 1, 1, 26, 57]),
            Bot(self.field, [17, 9, 1, 61, 1, 1, 26, 57]),
            Bot(self.field, [17, 9, 1, 61, 1, 1, 26, 57]),
        ]

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode((self.windowWidth, self.windowHeight), pygame.HWSURFACE)

        pygame.display.set_caption('Pygame pythonspot.com example')
        self._running = True

    def on_event(self, event):
        if event.type == QUIT:
            self._running = False

    def on_loop(self):
        pass

    def on_render(self):
        self._display_surf.fill((0, 0, 0))
        for bot in self.bots:
            pygame.draw.rect(self._display_surf, (255, 0, 0), pygame.Rect(bot.x * size, bot.y * size, size, size))
        pygame.display.flip()

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if not self.on_init():
            self._running = False

        while self._running:
            pygame.event.pump()
            keys = pygame.key.get_pressed()

            import ipdb; ipdb.set_trace()

            # if (keys[K_RIGHT]):
            #     self.player.moveRight()
            #
            # if (keys[K_LEFT]):
            #     self.player.moveLeft()
            #
            # if (keys[K_UP]):
            #     self.player.moveUp()
            #
            # if (keys[K_DOWN]):
            #     self.player.moveDown()
            #
            # if (keys[K_ESCAPE]):
            #     self._running = False

            self.on_loop()
            self.on_render()
        self.on_cleanup()


if __name__ == "__main__":
    theApp = App()
    theApp.on_execute()
